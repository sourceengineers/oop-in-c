
# manage include directories
set(SRC_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/src)

include_directories(
	${SRC_ROOT}
)

# Add sourcecode
set(OOP_EXAMPLES_SOURCES
    ${SRC_ROOT}/1_class/SimpleClass.c
	${SRC_ROOT}/2_interface/Runnable.c
	${SRC_ROOT}/2_interface/MockRunnable.c
	${SRC_ROOT}/3_list/RunnableList.c
	${SRC_ROOT}/4_multiple-inheritance/MaxCheck.c
	${SRC_ROOT}/5_base-class/ACheck.c
	${SRC_ROOT}/5_base-class/MinCheck.c
)

# Add tests
set(OOP_EXAMPLES_UNIT_TESTS
    ${SRC_ROOT}/1_class/TestSimpleClass.cpp
	${SRC_ROOT}/2_interface/TestRunnable.cpp
	${SRC_ROOT}/3_list/TestRunnableList.cpp
	${SRC_ROOT}/4_multiple-inheritance/TestMaxCheck.cpp
	${SRC_ROOT}/5_base-class/TestMinCheck.cpp
)

