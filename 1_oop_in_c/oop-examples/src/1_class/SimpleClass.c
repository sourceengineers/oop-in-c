/*!****************************************************************************************************************************************
 * @file         SimpleClass.c
 *
 * @copyright    � by Source Engineers GmbH, 2019
 *
 * @authors      Flurin B�hler <flurin.buehler@sourceengineers.com>
 *
 *****************************************************************************************************************************************/

#include "SimpleClass.h"
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct SimpleClass_PrivateData
{
	uint16_t idOfInstance;
} PrivateData;


SimpleClass_Handle SimpleClass_create(uint16_t idOfInstance)
{
    PrivateData* me = malloc(sizeof(PrivateData));
    assert(me != NULL);

    // initialize private variables
    me->idOfInstance = idOfInstance;

    return me;
}

uint16_t SimpleClass_getIdOfInstance(SimpleClass_Handle me)
{
    return me->idOfInstance;
}

void SimpleClass_destroy(SimpleClass_Handle me)
{
    free(me);
}
