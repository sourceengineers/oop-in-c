#include <gtest/gtest.h>

extern "C" {
    #include "SimpleClass.h"
}

class SimpleClassTest : public ::testing::Test
{
protected:
    SimpleClassTest()
        : Test()
    {
    }

    void SetUp() override
    {
        _simpleClass = SimpleClass_create(1u);
    }

    void TearDown() override
    {
        SimpleClass_destroy(_simpleClass);
    }

    virtual ~SimpleClassTest()
    {
    }

    SimpleClass_Handle _simpleClass;

};

TEST_F(SimpleClassTest, TestSimpleClassCreation)
{
    // check creation works
    EXPECT_NE(nullptr, _simpleClass);

    // call a member
    EXPECT_EQ(1u, SimpleClass_getIdOfInstance(_simpleClass));
}

TEST_F(SimpleClassTest, TestCreateTwoInstances)
{
    // check creation works
    EXPECT_NE(nullptr, _simpleClass);
    SimpleClass_Handle simpleClass2 = SimpleClass_create(2u);
    EXPECT_NE(nullptr, simpleClass2);

    // call a member
    EXPECT_EQ(1u, SimpleClass_getIdOfInstance(_simpleClass));
    EXPECT_EQ(2u, SimpleClass_getIdOfInstance(simpleClass2));

    // clean up at the end of the test
    SimpleClass_destroy(simpleClass2);
}
