/*!****************************************************************************************************************************************
 * @file         SimpleClass.h
 *
 * @copyright    © by Source Engineers GmbH, 2019
 *
 * @authors      Flurin Bühler <flurin.buehler@sourceengineers.com>
 *
 * @brief        TODO
 *
 *****************************************************************************************************************************************/

#ifndef SIMPLECLASS_H_
#define SIMPLECLASS_H_
#include <stdint.h>

/**
 * Declaration of the SimpleClass handle.
 */
typedef struct SimpleClass_PrivateData* SimpleClass_Handle;

/**
 * Creates a SimpleClass instance.
 */
SimpleClass_Handle SimpleClass_create(uint16_t idOfInstance);

/**
 * get the instance-id of the class
 */
uint16_t SimpleClass_getIdOfInstance(SimpleClass_Handle me);

/**
 * Destroys a SimpleClass instance.
 */
void SimpleClass_destroy(SimpleClass_Handle me);

#endif // SIMPLECLASS_H_
