/*!****************************************************************************************************************************************
 * @file         ACheck.h
 *
 * @copyright    © by Source Engineers GmbH, 2019
 *
 * @authors      Flurin Bühler <flurin.buehler@sourceengineers.com>
 *
 * @brief        TODO
 *
 *****************************************************************************************************************************************/

#ifndef ACHECK_H_
#define ACHECK_H_

#include "4_multiple-inheritance/ICheckable.h"
#include "2_interface/IRunnable.h"
#include <stdint.h>

/**
 * Declaration of the Template handle.
 */
typedef struct ACheck_ProtectedData* ACheck_Handle;

// Declare the handle to the child-class (void* because type of derived class is not known)
typedef void* ACheckChild_Handle;

// Declares the virtual methods of the base class
typedef bool (*ACheck_isCheckOk)(ACheckChild_Handle handle);

/**
 * Creates a ACheck instance.
 */
 ACheck_Handle ACheck_create(ACheckChild_Handle child, ACheck_isCheckOk checkFunction, uint16_t tripTimeMs, uint16_t callIntvervallMs);

/**
 * Destroys a ACheck instance.
 */
void ACheck_destroy(ACheck_Handle me);

/**
 * Returns a pointer to the ICheckable.
 */ 
ICheckable* ACheck_getICheckableInterface(ACheck_Handle me);

/**
 * Returns a pointer to the IRunnable.
 */
IRunnable* ACheck_getIRunnableInterface(ACheck_Handle me);


#endif // ACHECK_H_
