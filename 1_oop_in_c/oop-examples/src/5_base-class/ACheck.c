/*!****************************************************************************************************************************************
 * @file         ACheck.c
 *
 * @copyright    © by Source Engineers GmbH, 2019
 *
 * @authors      Flurin Bühler <flurin.buehler@sourceengineers.com>
 *
 * @brief        TODO
 *
 *****************************************************************************************************************************************/
#include "ACheckProtected.h"
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>


/**
 * The implementation of the ICheckable methods.
 * @{
 */
static void setActive(ICheckable_Handle handle, bool isActive);
static bool isCheckOk(ICheckable_Handle handle);
/** @} */

/**
 * The implementation of the IRunnable methods.
 * @{
 */
static void run(IRunnable_Handle handle);
/** @} */


ACheck_Handle ACheck_create(ACheckChild_Handle child, ACheck_isCheckOk checkFunction, uint16_t tripTimeMs, uint16_t callIntvervallMs)
{
    ACheckProtectedData* me = malloc(sizeof(ACheckProtectedData));
    assert(me != NULL);

    // initialize activable interface
    me->checkable.handle = me;
    me->checkable.setActive = &setActive;
    me->checkable.isCheckOk = &isCheckOk;

    // initialize runnable interface
    me->runnable.handle = me;
    me->runnable.run = &run;

    // initialize the virtual functions
    me->checkFunction = checkFunction;
    me->child = child;

    // initialize private variables
    me->isActive = false;
    me->isCheckOk = true;
    me->tripTimeMs = tripTimeMs;
    me->callIntvervallMs = callIntvervallMs;
    me->timeCheckNotOkMs = 0;

    return me;
}

void ACheck_destroy(ACheck_Handle me)
{
    free(me);
}


ICheckable* ACheck_getICheckableInterface(ACheck_Handle me)
{
    assert(me != NULL);
    return &me->checkable;
}

IRunnable* ACheck_getIRunnableInterface(ACheck_Handle me)
{
    assert(me != NULL);
    return &me->runnable;
}

static void setActive(ICheckable_Handle handle, bool isActive)
{
    ACheckProtectedData* me = (ACheckProtectedData*)handle;
    assert(me != NULL);
    me->isActive = isActive;
}

static bool isCheckOk(ICheckable_Handle handle)
{
    ACheckProtectedData* me = (ACheckProtectedData*)handle;
    assert(me != NULL);
    return me->isCheckOk;
}

static void run(IRunnable_Handle handle)
{
    ACheckProtectedData* me = (ACheckProtectedData*)handle;
    assert(me != NULL);

    if(me->isActive)
    {
        if(!me->checkFunction(me->child)) // call child-class with handle to child
        {
            me->timeCheckNotOkMs += me->callIntvervallMs;
        }
        else
        {
            // check is ok again
            me->timeCheckNotOkMs = 0;
            me->isCheckOk = true;
        }

        if(me->timeCheckNotOkMs >= me->tripTimeMs)
        {
            me->isCheckOk = false;
        }
    }
    else
    {
        me->isCheckOk = true;
        me->timeCheckNotOkMs = 0;
    }
}