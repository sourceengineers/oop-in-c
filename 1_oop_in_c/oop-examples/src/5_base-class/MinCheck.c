/*!****************************************************************************************************************************************
 * @file         MinCheck.c
 *
 * @copyright    © by Source Engineers GmbH, 2019
 *
 * @authors      Flurin Bühler <flurin.buehler@sourceengineers.com>
 *
 * @brief        TODO
 *
 *****************************************************************************************************************************************/
#include "MinCheck.h"
#include "ACheckProtected.h"
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>


typedef struct MinCheck_PrivateData
{
    ACheck_Handle base;
    float* valueToCheck;
    float tripLimit;
} PrivateData;


/**
 * The implementation of the virtual methods.
 * @{
 */
static bool isCheckOk(ACheckChild_Handle handle);
/** @} */



MinCheck_Handle MinCheck_create(float* valueToCheck, float tripLimit, uint16_t tripTimeMs, uint16_t callIntvervallMs)
{
    PrivateData* me = malloc(sizeof(PrivateData));
    assert(me != NULL);

    // initialize base-class, override virtual function
    me->base = ACheck_create(me,&isCheckOk,tripTimeMs,callIntvervallMs);

    // initialize private variables
    me->tripLimit = tripLimit;
    me->valueToCheck = valueToCheck;

    return me;
}

void MinCheck_destroy(MinCheck_Handle me)
{
    ACheck_destroy(me->base);
    free(me);
}

ICheckable* MinCheck_getICheckableInterface(MinCheck_Handle me)
{
    return ACheck_getICheckableInterface(me->base);
}

IRunnable* MinCheck_getIRunnableInterface(MinCheck_Handle me)
{
    return ACheck_getIRunnableInterface(me->base);
}


static bool isCheckOk(ACheckChild_Handle handle)
{
    PrivateData* me = (PrivateData*)handle;
    assert(me != NULL);

    bool isCheckOk = false;
    if(me->base->isActive) // use member from base-class
    {
        float value = *me->valueToCheck;
        if(value >= me->tripLimit)
        {
            isCheckOk = true;
        }
    }
    else
    {
        isCheckOk = true;
    }

    return isCheckOk;
}

