/*!****************************************************************************************************************************************
 * @file         ACheckProtected.h
 *
 * @copyright    © by Source Engineers GmbH, 2019
 *
 * @authors      Flurin Bühler <flurin.buehler@sourceengineers.com>
 *
 * @brief        Protected Header of ACheck
 *
 *****************************************************************************************************************************************/

#ifndef ACHECKPROTECTED_H_
#define ACHECKPROTECTED_H_

#include "ACheck.h"


// private data of ACheck. Declare this here to make the inheritance possible
typedef struct ACheck_ProtectedData
{
    ICheckable checkable;
    IRunnable runnable;
    ACheck_isCheckOk checkFunction;
    ACheckChild_Handle child;
    uint16_t tripTimeMs;
    uint16_t callIntvervallMs;
    uint16_t timeCheckNotOkMs;
    bool isActive;
    bool isCheckOk;
} ACheckProtectedData;


#endif // ACHECK_H_
