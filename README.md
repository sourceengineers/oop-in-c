oop-in-c
========

Slides, example-code and more for the “oop-in-c” workshop from [Source
Engineers.](https://www.sourceengineers.com/en)

 

Clone Repository
----------------

To clone this repositories use

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git clone https://bitbucket.org/sourceengineers/oop-in-c/src/master/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The repository is public - no login is required

 

Build Example-Code on Windows with Eclipse-CDT
----------------------------------------------

1.  Install [Cygwin,](https://www.cygwin.com/)select the following packages:

    -   cmake

    -   gcc-g++

    -   gdb

    -   git

    -   make

    -   openssh

2.  Install [Eclipse-CDT](https://www.eclipse.org/cdt/)

3.  Open and compile the manages eclipse-project in

    -   /1_oop_in_c/oop-examples

    -   /2_design_for_testability/dft-example

 

Build Example-Code on Windows with CLion
----------------------------------------

1.  Install [Cygwin,](https://www.cygwin.com/)select the following packages:

    -   cmake

    -   gcc-g++

    -   gdb

    -   git

    -   make

    -   openssh

2.  Install CLion

3.  Open and compile the CMakeLists.txt

    -   /1_oop_in_c/oop-examples/cmake

    -   /2_design_for_testability/dft-example/cmake

 

Use the Code Generation Scripts
-------------------------------

Install Python 3, then use the followin commands:

 

Generate an Interface

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python interface.py MyInterface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

Generate a Class

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python class.py MyClass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

Generate a Class that realizes an Interface

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python class.py MyClass MyInterface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

Generate a Mock

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python mock.py MyMock MyInterface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
