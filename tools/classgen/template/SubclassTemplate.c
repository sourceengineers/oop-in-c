/*!****************************************************************************************************************************************
 * @file         SubclassTemplateInclude.c
 *
 * @copyright    __COPYRIGHT__
 *
 * @authors      __AUTHOR__ <__EMAIL__>
 *
 * @brief        TODO
 *
 *****************************************************************************************************************************************/
#include "SubclassTemplateInclude.h"
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>


typedef struct SubclassTemplate_PrivateData
{
	InterfaceTemplate parent;
	uint16_t privateVariable;
} PrivateData;


/**
 * The implementation of the Parent methods.
 * @{
 */ 
static void function1(InterfaceTemplate_Handle handle);
/** @} */


SubclassTemplate_Handle SubclassTemplate_create(void)
{
    PrivateData* me = malloc(sizeof(PrivateData));
    assert(me != NULL);

    // initialize interface
    me->parent.handle = me;
    me->parent.function1 = &function1;

    // initialize private variables
    me->privateVariable = 0; 

    return me;
}

void SubclassTemplate_destroy(SubclassTemplate_Handle me)
{
    free(me);
}


InterfaceTemplate* SubclassTemplate_getInterfaceTemplateInterface(SubclassTemplate_Handle me)
{
    assert(me != NULL);
    return &me->parent;
}


static void function1(InterfaceTemplate_Handle handle)
{
    PrivateData* me = (PrivateData*)handle;
    assert(me != NULL);
}
