/*!****************************************************************************************************************************************
 * @file         ClassTemplateInclude.h
 *
 * @copyright    __COPYRIGHT__
 *
 * @authors      __AUTHOR__ <__EMAIL__>
 *
 * @brief        TODO
 *
 *****************************************************************************************************************************************/

#ifndef CLASSTEMPLATE_H_
#define CLASSTEMPLATE_H_


/**
 * Declaration of the ClassTemplate handle.
 */
typedef struct ClassTemplate_PrivateData* ClassTemplate_Handle;

/**
 * Creates a ClassTemplate instance.
 */
ClassTemplate_Handle ClassTemplate_create(void);

/**
 * Destroys a ClassTemplate instance.
 */
void ClassTemplate_destroy(ClassTemplate_Handle me);

#endif // CLASSTEMPLATE_H_
