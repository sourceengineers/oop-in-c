/*!****************************************************************************************************************************************
 * @file         MockTemplateInclude.h
 *
 * @copyright    __COPYRIGHT__
 *
 * @authors      __AUTHOR__ <__EMAIL__>
 *
 * @brief        TODO
 *
 *****************************************************************************************************************************************/

#ifndef MOCKTEMPLATE_H_
#define MOCKTEMPLATE_H_

#include "InterfaceTemplateInclude.h"
#include <stdint.h>

/**
 * Declaration of the mock object.
 */
typedef struct __MockTemplatePrivateData
{
	InterfaceTemplate parent;
	uint16_t privateVariable;
} MockTemplatePrivateData;


/**
 * initialize a MockTemplate instance.
 */
void MockTemplate_init(MockTemplatePrivateData* me);

/**
 * Returns a pointer to the InterfaceTemplate.
 */ 
InterfaceTemplate* MockTemplate_getInterfaceTemplateInterface(MockTemplatePrivateData* me);


#endif // MOCKTEMPLATE_H_
