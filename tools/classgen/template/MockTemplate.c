/*!****************************************************************************************************************************************
 * @file         MockTemplate.c
 *
 * @copyright    __COPYRIGHT__
 *
 * @authors      __AUTHOR__ <__EMAIL__>
 *
 * @brief        TODO
 *
 *****************************************************************************************************************************************/
#include "MockTemplateInclude.h"
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>


/**
 * The implementation of the Parent methods.
 * @{
 */ 
static void function1(InterfaceTemplate_Handle handle);
/** @} */


void MockTemplate_init(MockTemplatePrivateData* me)
{
    // initialize interface
    me->parent.handle = me;
    me->parent.function1 = &function1;

    // initialize private variables
    me->privateVariable = 0; 
}

InterfaceTemplate* MockTemplate_getInterfaceTemplateInterface(MockTemplatePrivateData* me)
{
    assert(me != NULL);
    return &me->parent;
}


static void function1(InterfaceTemplate_Handle handle)
{
    MockTemplatePrivateData* me = (MockTemplatePrivateData*)handle;
    assert(me != NULL);
}
