#include <gtest/gtest.h>

extern "C" {
    #include "ClassTemplateInclude.h"
}

class ClassNameTest : public ::testing::Test
{
protected:
    ClassNameTest()
        : Test()
    {
    }

    void SetUp() override
    {
        _className = ClassTemplate_create();
    }

    void TearDown() override
    {
    	ClassTemplate_destroy(_className);
    }

    virtual ~ClassNameTest()
    {
    }

    ClassTemplate_Handle _className;
};

TEST_F(ClassNameTest, ClassNameCreated)
{
	// test that the template was created
    EXPECT_NE(nullptr, _className);
}
