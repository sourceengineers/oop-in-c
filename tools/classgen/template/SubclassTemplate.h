/*!****************************************************************************************************************************************
 * @file         SubclassTemplateInclude.h
 *
 * @copyright    __COPYRIGHT__
 *
 * @authors      __AUTHOR__ <__EMAIL__>
 *
 * @brief        TODO
 *
 *****************************************************************************************************************************************/

#ifndef SUBCLASSTEMPLATE_H_
#define SUBCLASSTEMPLATE_H_

#include "InterfaceTemplateInclude.h"

/**
 * Declaration of the Template handle.
 */
typedef struct SubclassTemplate_PrivateData* SubclassTemplate_Handle;

/**
 * Creates a SubclassTemplate instance.
 */
SubclassTemplate_Handle SubclassTemplate_create(void);

/**
 * Destroys a SubclassTemplate instance.
 */
void SubclassTemplate_destroy(SubclassTemplate_Handle me);

/**
 * Returns a pointer to the InterfaceTemplate.
 */ 
InterfaceTemplate* SubclassTemplate_getInterfaceTemplateInterface(SubclassTemplate_Handle me);


#endif // SUBCLASSTEMPLATE_H_
