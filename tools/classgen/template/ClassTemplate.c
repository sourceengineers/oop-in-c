/*!****************************************************************************************************************************************
 * @file         ClassTemplateInclude.c
 *
 * @copyright    __COPYRIGHT__
 *
 * @authors      __AUTHOR__ <__EMAIL__>
 *
 *****************************************************************************************************************************************/

#include "ClassTemplateInclude.h"
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct ClassTemplate_PrivateData
{
	uint16_t privateVariable;
} PrivateData;


ClassTemplate_Handle ClassTemplate_create(void)
{
    PrivateData* me = malloc(sizeof(PrivateData));
    assert(me != NULL);

    // initialize private variables
    me->privateVariable = 0; 

    return me;
}

void ClassTemplate_destroy(ClassTemplate_Handle me)
{
    free(me);
}
