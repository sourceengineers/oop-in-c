/*!****************************************************************************************************************************************
 * @file         InterfaceTemplateInclude.h
 *
 * @copyright    __COPYRIGHT__
 *
 * @authors      __AUTHOR__ <__EMAIL__>
 *
 * @brief        TODO
 * 
 *****************************************************************************************************************************************/

#ifndef INTERFACETEMPLATE_H_
#define INTERFACETEMPLATE_H_

// Declare the handle to the interface
typedef void* InterfaceTemplate_Handle;

// Declares the methods of the interface
typedef void (*InterfaceTemplate_function1)(InterfaceTemplate_Handle handle);

// Declare the interface
typedef struct __InterfaceTemplate
{
    InterfaceTemplate_Handle handle;       // the explicit 'this' pointer
    InterfaceTemplate_function1 function1; // pointer to function implementation
} InterfaceTemplate;

#endif //INTERFACETEMPLATE_H_
