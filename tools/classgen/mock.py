# Copyright (C) by Source Engineers GmbH
#
# Script to generate C++ classes from templates
# TODO load author and email from json file, set date/year automatically
# TODO get path from parameter -> store stuff directly in correct folder
# TODO template with inheritance

import sys
import os
import FileGenerator

#-----------------------
# Main
#-----------------------

className = "NewClass"

if len( sys.argv ) >= 2:
    className = sys.argv[ 1 ]

    parentClass = ""
    if len( sys.argv ) >= 3:
        parentClass = sys.argv[ 2 ]

    # TODO add outPath as parameter outPath = sys.argv[ 2 ]
   
    path = os.getcwd()
    
    headerTemplate = os.path.join(path, "template", "MockTemplate.h")
    sourceTemplate = os.path.join(path, "template", "MockTemplate.c")
        
    fileName = className
    if fileName.rfind('_'):
        fileName = fileName[fileName.rfind('_')+1:]
    
    headerFile = os.path.join(path, fileName + ".h")
    classFile = os.path.join(path, fileName + ".c")

    FileGenerator.generateFile(headerTemplate, headerFile, className, parentClass )
    FileGenerator.generateFile(sourceTemplate, classFile, className, parentClass )

else:
    print ("invalid parameters: python classgen.py ClassName [ParentClassName]")
    print ("\n")
