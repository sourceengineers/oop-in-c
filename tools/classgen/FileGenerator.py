# Copyright (C) by Source Engineers GmbH

import sys
import os
import configparser

def generateFile( template, new, ClassName, ParentClassName, isTest = False ):
    print ("generating " + new)
    templateFile = open( template, 'r' )
    newFile = open( new, 'w', newline="\n" )
    lineCounter = 0
    line = templateFile.readline()
    
    author ='name'
    email ='email'
    licenseText = 'license'
    #read user information from ini file
    if os.path.isfile('./config/user.ini') :
        config = configparser.ConfigParser()
        config.read('./config/user.ini')
        author = config['User']['name']
        email = config['User']['email']
        licenseText = config['User']['license']
    else:
        print('user.ini not found')

    
    #TODO make only first letter lower
    variableName = "_" + ClassName.lower()
    
    if ClassName != "":
        includeProtection = ClassName.upper() + "_H_"
    else:
        includeProtection = ParentClassName.upper() + "_H_"
        
    handleName = "_" + ClassName.lower()
    
    classIncludeName = ClassName.replace('_','/')
    interfaceIncludeName = ParentClassName.replace('_','/')
                                   
    while len(line)>0:        
        #TODO add actual Year

        #replace include protections
        line = line.replace("SUBCLASSTEMPLATE_H_", includeProtection)
        line = line.replace("CLASSTEMPLATE_H_", includeProtection)
        line = line.replace("INTERFACETEMPLATE_H_", includeProtection)
        line = line.replace("MOCKTEMPLATE_H_", includeProtection)
        
        #variable in test-file
        line = line.replace("SubclassTemplateInclude", classIncludeName)
        line = line.replace("ClassTemplateInclude", classIncludeName)
        line = line.replace("MockTemplateInclude", classIncludeName)		
        line = line.replace("InterfaceTemplateInclude", interfaceIncludeName)
        
        #include line
        line = line.replace("_classTemplate", variableName)
        
        #user information
        line = line.replace("__AUTHOR__", author)
        line = line.replace("<__EMAIL__>", '<' + email + '>')
        line = line.replace("__COPYRIGHT__", licenseText)
                
        #replace class-names
        if ClassName != "":
            if isTest and ClassName.rfind('_'):
                TestClassName = ClassName[ClassName.rfind('_')+1:]     
                line = line.replace("ClassName", TestClassName)  
                TestClassVariable = TestClassName[0].lower() + TestClassName[1:]
                line = line.replace("className", TestClassVariable) 
            line = line.replace("SubclassTemplate", ClassName)          
            line = line.replace("ClassTemplate", ClassName) 
            line = line.replace("MockTemplate", ClassName) 
        if ParentClassName != "":
            line = line.replace("InterfaceTemplate", ParentClassName)    
        
        #write into new file
        newFile.write(line)
        line = templateFile.readline()
        lineCounter+=1
        
    newFile.close()
    templateFile.close()
