# Copyright (C) by Source Engineers GmbH
#
# Script to generate C++ classes from templates
# TODO load author and email from json file, set date/year automatically
# TODO get path from parameter -> store stuff directly in correct folder
# TODO template for interface
# TODO tempalte with inheritance

import sys
import os
import FileGenerator

#-----------------------
# Main
#-----------------------

interfaceName = "NewInterface"

if len( sys.argv ) >= 2:
    interfaceName = sys.argv[ 1 ]
    # TODO add outPath as parameter outPath = sys.argv[ 2 ]
   
    path = os.getcwd()
    headerTemplate = os.path.join(path, "template", "InterfaceTemplate.h")

    fileName = interfaceName
    if fileName.rfind('_'):
        fileName = fileName[fileName.rfind('_')+1:]

    headerFile = os.path.join(path, fileName + ".h")

    FileGenerator.generateFile(headerTemplate, headerFile, "", interfaceName )

else:
    print ("invalid parameters: python interface.py InterfaceName")
    print ("\n")
